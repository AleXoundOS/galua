{ stdenv, pkgs }:
let
  galua_lib_commit = "0f24be9bfc2e4c23da74e49b16d8494b72cbe2a8";
  galua_lib = pkgs.callPackage (builtins.fetchTarball {
    url = "https://gitlab.com/AleXoundOS/galua_lib/-/archive/${galua_lib_commit}.tar.gz";
    sha256 = "0hbzkj2ljdm7yl48nx8b6piz125g3glm7ma6j079kpnj10bl37g6";
  }) {};
in
stdenv.mkDerivation {
  name = "galua";
  src = builtins.path {path = ./.;};
  buildInputs = [ galua_lib ];
  buildPhase = ''
    gcc -c -o main.o sources/main.c
    gcc -o galua main.o ${galua_lib}/bin/galua_lib.so
  '';
  installPhase = ''
    mkdir -p $out/bin
    cp galua $out/bin
  '';
}
